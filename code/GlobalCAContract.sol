pragma solidity ^0.4.18;

contract GlobalCentralAuthority {
    struct User{
        string name;
    }
    mapping (address => User) map;
    address[] public userAccts;
    address public owner;
    
    function GlobalCentralAuthority(){
        owner = msg.sender;
    }
    
    modifier onlyOwner{
        if (msg.sender != owner) throw;
        _;
    }
    function storeID(address _address, string _name) onlyOwner {
        var user = map[_address];
        user.name = _name;
        userAccts.push(_address) -1;
    }
    function getID(address _address) view public returns (string) {
        return map[_address].name;
    }
}
