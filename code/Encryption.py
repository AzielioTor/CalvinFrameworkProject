
from Crypto.Hash import SHA256
from Crypto.Signature import PKCS1_v1_5
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP

class Encrypt:

    def encrypt(self,message,publickey):
        '''
        :param message: the byte array of the message text
        :param publickey: from key.publickey().export_key(), which belongs to <class 'bytes'>.
        :return: the encrypted message (bytes)
        '''
        public_key = RSA.import_key(publickey) # Cannot use the publickey directly. Need to import_key first.
        encryptor = PKCS1_OAEP.new(public_key)
        encrypted = encryptor.encrypt(message)
        return encrypted

    def decrypt(self,encrypted,privatekey):
        '''
        :param encrypted: encrypted message (bytes)
        :param privatekey: from key.export() (bytes)
        :return: decrpted message (bytes). The output should be b'the content of the message'
        '''
        private_key = RSA.import_key(privatekey) # Cannot use the privatelickey directly. Need to import_key first.
        decryptor = PKCS1_OAEP.new(private_key)
        decrypted = decryptor.decrypt(encrypted)
        return decrypted

    def signHash(self,message,privatekey):
        '''
        :param message: the message to be sent (bytes), which is the encoded plain text
        :param privatekey: from key.export()
        :return: signed hashed message
        '''
        private_key = RSA.import_key(privatekey)
        digest = SHA256.new()
        digest.update(message)
        signer = PKCS1_v1_5.new(private_key)
        signed = signer.sign(digest)
        return signed

    def verifySigned(self,message,signed,publickey):
        '''
        :param message: decrypted message(bytes)
        :param signed: signed hashed message(bytes)
        :param publickey: the public key to decrypted signed hased message
        :return: the hased value of the message is same as that of signed, return true; otherwise return false
        '''
        public_key = RSA.import_key(publickey)
        digest = SHA256.new()
        digest.update(message)
        verifier = PKCS1_v1_5.new(public_key)
        verified = verifier.verify(digest, signed)
        return verified

    def processReceived(self,receivedMessage,publickey,privatekey):
        '''
        :param receivedMessage: encrypted message + signed hashed message (256 bytes)
        :param publickey: the public key to unsign the signed data (the public key of the sender)
        :param privatekey: the privatekey to decrypt the encrypted message (own private key)
        :return: the decrypted message and verified result(true or false)
        '''
        encrypted = receivedMessage[0:128]
        signed = receivedMessage[128:]
        decrypted = self.decrypt(encrypted,privatekey)
        verified = self.verifySigned(decrypted,signed,publickey)
        return decrypted,verified

    def sentData(self,message,publickey,privatekey):
        '''
        :param message: the messsage to be sent (bytes)
        :param publickey: recipient's public key to encrypt the message
        :param privatekey: own private key to sign the hahsed message
        :return:  encrypted message + signed hashed message
        '''
        encrypted = self.encrypt(message,publickey)
        signed = self.signHash(message,privatekey)
        sentData = encrypted+signed
        return sentData

'''
def main():
    messageText = 'this is the message'
    message = messageText.encode()
    key1 = RSA.generate(1024)
    privatekey1 = key1.exportKey()
    publickey1 = key1.publickey().exportKey()

    key2 = RSA.generate(1024)
    privatekey2 = key2.exportKey()
    publickey2 = key2.publickey().exportKey()

    sentdata = sentData(message,publickey2,privatekey1)
    print(sentdata)
    receivedMessage = sentdata
    print(processReceived(receivedMessage,publickey1,privatekey2))



if __name__ == '__main__':
    main()
'''