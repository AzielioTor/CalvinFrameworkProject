import web3
import json

from web3 import Web3
from CA import CertificateAuthority
from Crypto.PublicKey import RSA
from Encryption import Encrypt
import socket
import sys

def main():
    '''
    Input Node Server ip , port and CA server ip, port
    Register with CA and accept connection from a host
    :return:
    '''
    # Test network address
    w3 = Web3(Web3.HTTPProvider("http://172.17.0.2:8545"))
    accountAddr = w3.eth.accounts[2]
    key = RSA.generate(1024)
    # Generate private key
    priv_key = key.exportKey()
    # Generate public key
    pub_key = key.publickey().exportKey()
    # Register with CA
    contractAccount = register(accountAddr,sys.argv[3],int(sys.argv[4]), pub_key)
    # Create a connection
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (sys.argv[1], int(sys.argv[2]))
    print("Starting up server at port",sys.argv[2])
    sock.bind(server_address)
    while True:
        # Listen for incoming connections
        sock.listen(1)
        print('waiting for a connection')
        connection, client_address = sock.accept()
        try:
            encrypt = Encrypt()
            print('connection from', client_address)
            # Receive request
            req = connection.recv(7).decode()
            if req == 'request':
                # Receive client account address
                clientAddr = connection.recv(42).decode()
                # Send account address
                connection.send(accountAddr.encode('utf-8'))
                # Get hosts public key
                hostkey = getHostPublicKey(contractAccount, accountAddr, clientAddr)
                if hostkey != '':
                    # Encrypt data
                    data = encrypt.sentData(accountAddr.encode('utf-8'), hostkey, priv_key)
                    # Receive enrypted data
                    rcv = connection.recv(256)
                    # Send encrypted data
                    connection.send(data)
                    # Decrypt and verify signature
                    rcv_data, verify = encrypt.processReceived(rcv, hostkey, priv_key)
                    if verify:
                        print('Connection established')
        finally:
            print('Closing connection')
            connection.close()


def register(accountaddr, ip, port, pk):
    '''
    Connects to contract owner and registers
    :param accountaddr: User account address
    :param ip: owner ip
    :param port: owner port
    :return: contract address
    '''
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Connect the socket to the port where the server is listening
    contractaddr = ''
    server_address = (ip, port)
    print('Connecting to port', port)
    sock.connect(server_address)
    try:
        sock.send(accountaddr.encode('utf-8'))
        sock.send(pk)
        contractaddr = sock.recv(42).decode()

    finally:
        print('Closing connection')
        sock.close()
    return contractaddr

def getHostPublicKey(contractaddr,myaddr,hostaddr):
    '''
    Retrieve hosts public key
    :param contractaddr: Contract address
    :param myaddr: User address
    :param hostaddr: Host address
    :return: key
    '''
    ca = CertificateAuthority("172.17.0.2", "8545", contractaddr)
    key = ca.requestClientCertificate(myaddr, hostaddr)
    return key


if __name__ == '__main__':
    main()
