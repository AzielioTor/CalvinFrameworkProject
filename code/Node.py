import web3
import json

from web3 import Web3
from CA import CertificateAuthority
from Crypto.PublicKey import RSA
from Encryption import Encrypt
import socket
import sys

def main():
    '''
    Input CA server ip and port
    Register with CA and request connection with a host
    :return:
    '''
    # Test network address
    w3 = Web3(Web3.HTTPProvider("http://172.17.0.2:8545"))
    accountAddr = w3.eth.accounts[1]
    key = RSA.generate(1024)
    # Generate private key
    priv_key = key.exportKey()
    # Generate public key
    pub_key = key.publickey().exportKey()
    # Register with CA
    contractAccount = register(accountAddr,sys.argv[1],int(sys.argv[2]), pub_key)
    while True:
        host = input("Enter host ip:")
        hostport = int(input("Enter host port:"))
        # Socket connection to the host
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = (host, hostport)
        print('Connecting to port', hostport)
        sock.connect(server_address)
        try:
            encrypt = Encrypt()
            # Send request
            sock.send('request'.encode('utf-8'))
            # Send account address
            sock.send(accountAddr.encode('utf-8'))
            # Receive client account address
            clientAddr = sock.recv(42).decode()
            # Get hosts public key
            hostkey = getHostPublicKey(contractAccount,accountAddr,clientAddr)
            # Encrypt data
            data = encrypt.sentData(accountAddr.encode('utf-8'), hostkey, priv_key)
            # Send encrypted data
            sock.send(data)
            # Receive enrypted data
            rcv = sock.recv(256)
            # Decrypt and verify signature
            rcv_data, verify = encrypt.processReceived(rcv, hostkey, priv_key)
            if verify:
                print('Connection established')
        finally:
            print('Closing connection')
            sock.close()

def register(accountaddr, ip, port, pk):
    '''
    Connects to contract owner and registers
    :param accountaddr: User account address
    :param ip: owner ip
    :param port: owner port
    :return: contract address
    '''
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Connect the socket to the port where the server is listening
    contractaddr = ''
    server_address = (ip, port)
    print('Connecting to port', port)
    sock.connect(server_address)
    try:
        sock.send(accountaddr.encode('utf-8'))
        sock.send(pk)
        contractaddr = sock.recv(42).decode()

    finally:
        print('Closing connection')
        sock.close()
    return contractaddr

def getHostPublicKey(contractaddr,myaddr,hostaddr):
    '''
    Retrieve hosts public key
    :param contractaddr: Contract address
    :param myaddr: User address
    :param hostaddr: Host address
    :return: key
    '''
    ca = CertificateAuthority("172.17.0.2", "8545", contractaddr)
    key = ca.requestClientCertificate(myaddr, hostaddr)
    return key


if __name__ == '__main__':
    main()
