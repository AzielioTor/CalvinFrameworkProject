import web3
import json

from web3 import Web3
class CertificateAuthority:
    __slots__ = 'ipaddr','address','port'

    def __init__(self,ip,port,addr):
        # Network IP
        self.ipaddr = ip
        # Contract address
        self.address = addr
        # Network port
        self.port = port

    def requestCeritificate(self,owner,client,key):
        '''
        Stores users key at users address
        :param user: user address
        :param key: public key
        :return:
        '''
        w3,contract = self.getContract()
        txs = contract.functions.storeID(client, key).transact(
            {'to': self.address, 'from': owner})
        txs_receipt = w3.eth.getTransactionReceipt(txs)
        print(txs_receipt)


    def getContract(self):
        '''
        Retrieve contract deployed at the address
        :return: web3 object, contract object
        '''
        w3 = Web3(Web3.HTTPProvider("http://"+self.ipaddr+":"+self.port))
        with open('abi', 'r') as abi_definition:
            abi = json.load(abi_definition)
        contract = w3.eth.contract(address=w3.toChecksumAddress(self.address), abi=abi)
        return (w3,contract)

    def requestClientCertificate(self,user,client):
        '''
        Returns clients public key, requested by user
        :param client: client address
        :param user: user address
        :return: clients public key
        '''
        w3, contract = self.getContract()
        key = contract.functions.getID(client).call(
            {'to': self.address, 'from': user})
        return key
