#!/bin/bash
echo "Installing python3.5..."
wget https://www.python.org/ftp/python/3.5.5/Python-3.5.5.tgz
tar xzf Python-3.5.5.tgz
cd Python-3.5.5
./configure --enable-optimizations
make altinstall
python3.5 -V
echo "Installing web3..."
pip3.5 install web3
python3.5 -V
pip3.5 -V
pip3.5 show web3
echo "Removing unneeded files..."
rm Python-3.5.5.tgz
echo "Completed Installation of Python3.5 & web3!"
