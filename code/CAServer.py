import socket
import sys
import web3
import json

from CA import CertificateAuthority
from web3 import Web3

def main():
    '''
    Input CA Server ip and port
    :return:
    '''
    w3 = Web3(Web3.HTTPProvider("http://172.17.0.2:8545"))
    # Owner address
    w3.eth.defaultAccount = w3.eth.accounts[0]
    serverAccount = w3.eth.defaultAccount
    with open('abi', 'r') as abi_definition:
        abi = json.load(abi_definition)
    # contract details
    contract = w3.eth.contract(address=w3.toChecksumAddress(w3.eth.defaultAccount), abi=abi,
                               bytecode='608060405234801561001057600080fd5b5033600260006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055506105ac806100616000396000f300608060405260043610610062576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff1680630ab2bb13146100675780638da5cb5b146100d457806399f826a51461012b578063cad2e392146101e7575b600080fd5b34801561007357600080fd5b5061009260048036038101908080359060200190929190505050610270565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b3480156100e057600080fd5b506100e96102ae565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b34801561013757600080fd5b5061016c600480360381019080803573ffffffffffffffffffffffffffffffffffffffff1690602001909291905050506102d4565b6040518080602001828103825283818151815260200191508051906020019080838360005b838110156101ac578082015181840152602081019050610191565b50505050905090810190601f1680156101d95780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b3480156101f357600080fd5b5061026e600480360381019080803573ffffffffffffffffffffffffffffffffffffffff169060200190929190803590602001908201803590602001908080601f01602080910402602001604051908101604052809392919081815260200183838082843782019150505050505091929192905050506103b7565b005b60018181548110151561027f57fe5b906000526020600020016000915054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b600260009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b60606000808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156103ab5780601f10610380576101008083540402835291602001916103ab565b820191906000526020600020905b81548152906001019060200180831161038e57829003601f168201915b50505050509050919050565b6000600260009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561041557600080fd5b6000808473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002090508181600001908051906020019061046d9291906104db565b506001808490806001815401808255809150509060018203906000526020600020016000909192909190916101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055505050505050565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061051c57805160ff191683800117855561054a565b8280016001018555821561054a579182015b8281111561054957825182559160200191906001019061052e565b5b509050610557919061055b565b5090565b61057d91905b80821115610579576000816000905550600101610561565b5090565b905600a165627a7a7230582004d3a7f030aee9b6a11d0e2cb9a91fd74f680b3262da7ee687c317d253c8d1590029')

    tc = contract.constructor().transact()
    tx_receipt = w3.eth.getTransactionReceipt(tc)
    # store contract address
    print(tx_receipt['contractAddress'])
    contractAddr = tx_receipt['contractAddress']
    # CA object
    ca = CertificateAuthority("172.17.0.2", "8545", contractAddr)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (sys.argv[1], int(sys.argv[2]))
    print("Starting up server at port",sys.argv[2])
    sock.bind(server_address)
    # Listen for incoming connections
    sock.listen(1)

    while True:
        # Wait for a connection
        print('waiting for a connection')
        connection, client_address = sock.accept()
        try:
            print('connection from', client_address)
            clientAccount = connection.recv(42).decode()
            data = connection.recv(1024).decode()
            #ca.requestCeritificate(contract,w3.eth.defaultAccount,clientAccount,data)
            print(clientAccount)
            txs = contract.functions.storeID(clientAccount, data).transact(
                {'to': contractAddr, 'from': serverAccount})
            txs_receipt = w3.eth.getTransactionReceipt(txs)
            print(txs_receipt)
            connection.send(contractAddr.encode('utf-8'))

        finally:
            # Clean up the connection
            connection.close()


if __name__ == '__main__':
    main()
